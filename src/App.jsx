import reactLogo from './assets/react.svg'
import './App.css'
import { useDispatch, useSelector } from 'react-redux'
import { decrement, increment, incrementBy } from './store/slices/counter/counterSlices';

function App() {
  // Leer del Store
  const { counter } = useSelector(state => state.counter);
  // Acciones
  const dispatch = useDispatch();

  return (
    <>
      <div>
        <a href="https://react.dev" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>count is {counter}</h1>
      <div className="card">
        <button onClick={() => dispatch(increment())}>
          Incrementar
        </button>
        <button onClick={() => dispatch(decrement())}>
          Decrementar
        </button>
        <button onClick={() => dispatch(incrementBy(2))}>
          Incrementar en 2
        </button>
      </div>
    </>
  )
}

export default App
